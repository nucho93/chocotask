new Vue ({
    el: '#app',
    data: {
        items: [],
		directFlights: [],
		inDirectFlights: [],
		airlines: [],
		airlineFilters: [],
		directAirlineFlights: {},
		inDirectAirlineFlights: {},
		hopFlight: true,
		allFlight: true,
		showFilter: false,
    },
	computed: {
		filteredItems: function () {
			if (this.allFight) {
				return this.items
			}
			else {
				let result = [];
				for (let i = 0; i < this.items.length; i++) {
					if (this.airlineFilters.includes(this.items[i].flights[0].airline.name)) {
						result.push(this.items[i])
					}
				}
				return result;
			}
		}
	},
	methods: {
		flightDuration(x,y){
			dTime=x.split(":")
			dHour=parseInt(dTime[0])*60
			dMinute=parseInt(dTime[1])
			dAll=dHour+dMinute
			
			aTime=y.split(":")
			aHour=parseInt(aTime[0])*60
			aMinute=parseInt(aTime[1])
			aAll=aHour+aMinute
			
			if(aAll>=dAll){
				duration=aAll-dAll
			}else{
				duration=(1440+aAll)-dAll
			}
			
			h=parseInt(duration/60)
			m=duration%60
			if(h>0){
				return(h+"ч "+m+"м")
			}else{
				return(m+"м")
			}
		},
		hopFlights(){
			this.hopFlight=!this.hopFlight
			if(this.hopFlight){
				this.items=this.directFlights
			}else{
				this.items=this.inDirectFlights
			}
		},
		toggleAll(){
			if(this.allFlight){
				this.airlineFilters=[]
				this.allFlight=false
			}else{
				this.airlineFilters=this.airlines
				this.allFlight=true
			}
			console.log(this.allFlight)
		},
		myShow(){
			this.showFilter=!this.showFilter
			console.log(this.showFilter)
		},
	},
    mounted(){
        fetch("http://vueapp.nucho.kz/data.json")
		.then(response => response.json())
        .then((data) => {
            this.items = data;
			this.directFlights = data;
			for(i=0;i<data.length;i++){		
				flight=data[i]
				flightName=flight.flights[0].airline.name
				
				if(flight.flights.length==1){
					this.inDirectFlights.push(flight)
				}
				
				if(this.airlines.indexOf(flightName)==-1){
					this.airlines.push(flightName)
					this.airlineFilters.push(flightName)
				}			
				
				this.inDirectAirlineFlights[flightName]=[]
				this.directAirlineFlights[flightName]=[]
			}
			
			for(i=0;i<data.length;i++){	
				flight=data[i]
				flightName=flight.flights[0].airline.name
				
				if(this.airlineFilters.indexOf(flightName)!=-1){
					this.inDirectAirlineFlights[flightName].push(flight)
				}
				if(flight.flights.length==1 && this.airlineFilters.indexOf(flightName)!=-1){
					this.directAirlineFlights[flightName].push(flight)
				}
			}
        })
    }
});

